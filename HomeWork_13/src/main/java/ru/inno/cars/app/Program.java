package ru.inno.cars.app;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import com.zaxxer.hikari.HikariDataSource;
import ru.inno.cars.models.Car;
import ru.inno.cars.repository.CarsRepository;
import ru.inno.cars.repository.CarsRepositoryJdbcImpl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;

@Parameters(separators = "=")
public class Program {

    @Parameter(names = {"-action"})
    private List<String> files;

    public static void main(String[] args) {
        String action = args[0];
        Properties dbProperties = new Properties();
        try {
            dbProperties.load(new BufferedReader(
                    new InputStreamReader(Program.class.getResourceAsStream("/db.properties"))));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        HikariDataSource dataSource = new HikariDataSource();
        dataSource.setPassword(dbProperties.getProperty("db.password"));
        dataSource.setUsername(dbProperties.getProperty("db.username"));
        dataSource.setJdbcUrl(dbProperties.getProperty("db.url"));
        dataSource.setMaximumPoolSize(Integer.parseInt(dbProperties.getProperty("db.hikari.maxPoolSize")));

        CarsRepository carsRepository = new CarsRepositoryJdbcImpl(dataSource);

        if (action.equals("read")) {
            List<Car> cars = carsRepository.findAll();
            for (Car car : cars) {
                System.out.println(car);
            }
        } else if (action.equals("write")) {
            Scanner scan = new Scanner(System.in);
            while (true) {
                System.out.println("Car type: ");
                String carType = scan.nextLine();
                System.out.println("Color: ");
                String color = scan.nextLine();
                System.out.println("Car number: ");
                String carNumber = scan.nextLine();
                Car car = Car.builder()
                        .carType(carType)
                        .color(color)
                        .carNumber(carNumber)
                        .build();
                carsRepository.save(car);
                String field = scan.nextLine();
                if (field.equalsIgnoreCase("exit")) {
                    return;
                }
            }
        }
    }
}