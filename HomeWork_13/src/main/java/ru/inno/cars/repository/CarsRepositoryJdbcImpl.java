package ru.inno.cars.repository;

import ru.inno.cars.models.Car;
import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public class CarsRepositoryJdbcImpl implements CarsRepository {

    //language=SQL
    private static final String SQL_SELECT_ALL = "select * from car order by id";

    //language=SQL
    private static final String SQL_INSERT = "insert into car(car_type, color, car_number) " +
            "values (?,?,?)";
    private DataSource dataSource;

    public CarsRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }


    public static final Function<ResultSet, Car> carRowMapper = row -> {
        try {
            return Car.builder()
                    .id(row.getLong("id"))
                    .carType(row.getString("car_type"))
                    .color(row.getString("color"))
                    .carNumber(row.getString("car_number"))
                    .idCarOwner(row.getLong("id_car_owner"))
                    .build();
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    };

    @Override
    public List<Car> findAll() {
        List<Car> cars = new ArrayList<>();
        try (Connection connection = dataSource.getConnection();
             Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(SQL_SELECT_ALL)) {
                while (resultSet.next()) {
                    Car car = carRowMapper.apply(resultSet);
                    cars.add(car);
                }
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
        return cars;
    }

    @Override
    public void save(Car car) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setString(1, car.getCarType());
            preparedStatement.setString(2, car.getColor());
            preparedStatement.setString(3, car.getCarNumber());

            int affectedRows = preparedStatement.executeUpdate();
            if (affectedRows != 1) {
                throw new SQLException("Unsuccessful insert");
            }

            try (ResultSet generatedId = preparedStatement.getGeneratedKeys()) {
                if (generatedId.next()) {
                    car.setId(generatedId.getLong("id"));
                } else {
                    throw new SQLException("Can't obtain generated id");
                }
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }
}
