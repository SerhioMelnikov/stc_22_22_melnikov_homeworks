package ru.inno.cars.models;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

public class Car {
    private Long id;
    private String carType;
    private String color;
    private String carNumber;
    private Long idCarOwner;
}
