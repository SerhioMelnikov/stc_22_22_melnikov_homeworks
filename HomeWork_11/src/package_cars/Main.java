package package_cars;
import java.io.IOException;
public class Main {

    public static void main(String[] args) throws IOException {
        CarsRepository carsRepository = new CarsRepositoryImpl("input.txt");
        CarService carService = new CarService(carsRepository);

        System.out.println("Всего машин: " + carService.countOfAllCars());
        System.out.println(carsRepository.blackColorOrZeroMileage("Black", 0));
        System.out.println(carsRepository.minPrice());
        System.out.println("Уникальных моделей в заданном диапозоне: " +
                carsRepository.uniqueCarTypes(0, 250_000));
        System.out.println(carsRepository.averageCostOfCars("Camry"));
    }
}