package package_cars;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.*;
import java.util.function.Function;


public class CarsRepositoryImpl implements CarsRepository {
    private final String fileName;

    public CarsRepositoryImpl(String fileName) {
        this.fileName = fileName;
    }

    private static final Function<String, Car> stringToCarMapper = currentCar -> {
        String[] parts = currentCar.split("\\|");
        String number = parts[0];
        String carType = parts[1];
        String color = parts[2];
        Integer mileage = Integer.parseInt(parts[3]);
        Integer price = Integer.parseInt(parts[4]);
        return new Car(number, carType, color, mileage, price);
    };

    @Override
    public List<Car> findAll() {
        List<Car> cars = new ArrayList<>();
        try (Reader fileReader = new FileReader(fileName);
             BufferedReader reader = new BufferedReader(fileReader)) {
            String currentCar = reader.readLine();
            while (currentCar != null) {
                Car car = stringToCarMapper.apply(currentCar);
                cars.add(car);
                currentCar = reader.readLine();
            }
        } catch (IOException e) {
            System.out.println("Ошибка ввода-вывода");
        }
        return cars;
    }

    @Override
    public List<Car> blackColorOrZeroMileage(String color, int mileage) {
        List<Car> cars = new ArrayList<>();
        try (Reader fileReader = new FileReader(fileName);
             BufferedReader reader = new BufferedReader(fileReader)) {
            String currentCar = reader.readLine();
            while (currentCar != null) {
                Car car = stringToCarMapper.apply(currentCar);
                if (car.getColor().equals(color) || car.getMileage() == mileage) {
                    cars.add(car);
                }
                currentCar = reader.readLine();
            }
        } catch (IOException e) {
            System.out.println("Ошибка ввода-вывода");
        }
        return cars;
    }

    @Override
    public long uniqueCarTypes(int mileageFrom, int mileageTo) {
        try (BufferedReader reader = new BufferedReader((new FileReader(fileName)))) {
            return reader.lines()
                    .map(stringToCarMapper)
                    .filter(car -> car.getPrice() > mileageFrom && car.getPrice() < mileageTo)
                    .map(Car::getCarType)
                    .distinct()
                    .count();
        } catch (IOException e) {
            System.out.println("Ошибка ввода-вывода");
        }
        return 0;
    }

    @Override
    public Optional<Car> minPrice() {
        try (BufferedReader reader = new BufferedReader((new FileReader(fileName)))) {
            return reader.lines()
                    .map(stringToCarMapper)
                    .min(Comparator.comparing(Car::getPrice));
        } catch (IOException e) {
            System.out.println("Ошибка ввода-вывода");
        }
        return Optional.empty();
    }

    @Override
    public OptionalDouble averageCostOfCars(String carType) {
        try (BufferedReader reader = new BufferedReader((new FileReader(fileName)))) {
            return reader.lines()
                    .map(stringToCarMapper)
                    .filter(car -> car.getCarType().equals(carType))
                    .mapToInt(Car::getPrice)
                    .average();
        } catch (IOException e) {
            System.out.println("Ошибка ввода-вывода");
        }
        return OptionalDouble.empty();
    }
}







