package package_cars;

import java.util.Objects;

public class Car {private String number;private String carType;
    private String color;
    private Integer mileage;
    private Integer price;

    public Car(String number, String carType, String color, Integer mileage, Integer price) {
        this.number = number;
        this.carType = carType;
        this.color = color;
        this.mileage = mileage;
        this.price = price;
    }

    public String getNumber() {
        return number;
    }
    public String getCarType() {
        return carType;
    }
    public String getColor() {
        return color;
    }
    public Integer getMileage() {
        return mileage;
    }
    public Integer getPrice() {
        return price;
    }
    @Override
    public String toString() {
        return "Car{" +
                "number='" + number + '\'' +
                ", carType='" + carType + '\'' +
                ", color='" + color + '\'' +
                ", mileage=" + mileage +
                ", price=" + price +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return Objects.equals(number, car.number) && Objects.equals(carType, car.carType) && Objects.equals(color, car.color) && Objects.equals(mileage, car.mileage) && Objects.equals(price, car.price);
    }

    @Override
    public int hashCode() {
        return Objects.hash(number, carType, color, mileage, price);
    }
}
