package package_cars;

import java.util.List;
import java.util.Optional;
import java.util.OptionalDouble;

public interface CarsRepository {
    List<Car> findAll();
    List<Car> blackColorOrZeroMileage(String color, int mileage);
    long uniqueCarTypes(int mileageFrom, int mileageTo);
    Optional<Car> minPrice();
    OptionalDouble averageCostOfCars(String carType);

}
