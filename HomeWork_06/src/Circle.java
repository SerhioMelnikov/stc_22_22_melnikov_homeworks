public class Circle extends Figure {

    protected double smallRadius;

    public Circle(String name, double x, double y, double smallRadius) {
        super(name, x, y);
        if (smallRadius > 0) {
            this.smallRadius = smallRadius;
        } else {
            this.smallRadius = DEFAULT_SIZE;
        }
    }
    public double getArea() {
        return Math.PI * Math.pow(smallRadius, 2);
    }
    public double getPerimeter() {
        return 2 * Math.PI * smallRadius;
    }

}

