public class Ellipse extends Circle {

    private double bigRadius;

    public Ellipse(String name, double x, double y, double smallRadius, double bigRadius) {
        super(name, x, y, smallRadius);
        if (bigRadius > 0) {
            this.bigRadius = bigRadius;
        } else {
            this.bigRadius = DEFAULT_SIZE *2;
        }

    }
    @Override
    public double getArea() {
        return Math.PI * smallRadius * bigRadius;
    }

    @Override
    public double getPerimeter() {
        return 4 * ((Math.PI * smallRadius * bigRadius + ((Math.pow(smallRadius - bigRadius, 2)))) / (smallRadius + bigRadius));
    }


}
