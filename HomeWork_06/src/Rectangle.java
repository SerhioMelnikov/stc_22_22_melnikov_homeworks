public class Rectangle extends Square {

    private double sideB;

    public Rectangle(String name, double x, double y, double sideA, double sideB) {
        super(name, x, y, sideA);
        if (sideB > 0) {
            this.sideB = sideB;
        } else {
            this.sideB = DEFAULT_SIZE * 2;
        }
    }
    @Override
    public double getArea() {
        return sideA * sideB;
    }
    @Override
    public double getPerimeter() {
        return 2 * (sideA + sideB);
    }


}
