public class Figure {
    private String name;
    protected double x; // координаты центра
    protected double y;
    protected final double DEFAULT_SIZE = 10; // дефолтное значение для длины/ширины/радиуса


    public Figure(String name, double x, double y) {
        this.name = name;
        this.x = x;
        this.y = y;
    }
    public void move(double toX, double toY) {
        this.x += toX;
        this.y += toY;
    }

}
