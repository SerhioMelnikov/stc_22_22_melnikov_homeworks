public class Square extends Figure {
    protected double sideA;


    public Square(String name, double x, double y, double sideA) {
        super(name, x, y);
        if (sideA > 0) {
            this.sideA = sideA;
        } else {
            sideA = DEFAULT_SIZE;
        }
        this.sideA = sideA;
    }

    public double getArea() {
        return Math.pow(sideA, 2);
    }

    public double getPerimeter() {
        return 4 * sideA;
    }
}

