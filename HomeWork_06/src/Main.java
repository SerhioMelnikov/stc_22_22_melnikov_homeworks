public class Main {

    public static void main(String[] args) {
        Square mySquare1 = new Square("mySq", 3, 4, 5);
        System.out.println(mySquare1.getArea());
        System.out.println(mySquare1.getPerimeter());

        Rectangle myRect = new Rectangle("myRect", 7, 7,5, 8);
        System.out.println(myRect.getArea());

        Ellipse myEllipse1 = new Ellipse("myEll", 4,2, 5, 7);
        Circle myCircle1 = new Circle("myCirc", 5, 6, 5);

        System.out.println(myEllipse1.getPerimeter());
        System.out.println(myCircle1.getPerimeter());

        mySquare1.move(2, 5);

    }




}