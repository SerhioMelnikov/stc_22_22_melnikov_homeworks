import java.util.Arrays;

public class ArraysTasksResolver implements ArrayTask {
    static void resolveTask(int[] array, ArrayTask task, int from, int to) {
        System.out.println(Arrays.toString(array));
        System.out.println("from:" + from + " " + "to:" + to + " ");
        System.out.println("result:" + task.resolve(array, from, to));
    }

    @Override
    public int resolve(int[] array, int from, int to) {
        return 0;
    }
}


