public class Main {

    public static void main(String[] args) {
        int[] myArray = {12, 62, 4, 2, 100, 40, 56};

        ArrayTask maxElement = (array, from, to) -> {
            if (from < 0 || to > array.length - 1) {
                System.err.println("Значения за границей диапазона");
                return 0;
            } else {
                int maxDigit = array[from];
                for (int i = from; i <= to; i++) {
                    //maxDigit= Math.max(maxDigit, array[i]);
                    if (array[i] > maxDigit) {
                        maxDigit = array[i];
                    }
                }
                int sum = 0;
                // for ( ; maxDigit > 0; maxDigit /= 10){
                //    sum += maxDigit % 10}
                while (maxDigit != 0) {
                    sum += maxDigit % 10;
                    maxDigit /= 10;
                }
                return sum;

            }
        };

        ArrayTask sumNumbersFromTo = (array, from, to) -> {
            if (from < 0 || to > array.length - 1) {
                System.err.println("Значения за границей диапазона");
                return 0;
            } else {
                int sum = 0;
                for (int i = from; i <= to; i++) {
                    sum += array[i];
                }
                return sum;
            }
        };

        ArraysTasksResolver.resolveTask(myArray, sumNumbersFromTo, 1, 5);
        ArraysTasksResolver.resolveTask(myArray, maxElement, 0, 3);


    }

}

