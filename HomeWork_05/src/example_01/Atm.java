package example_01;

public class Atm {
    private int currentBalance; //Сумма оставшихся денег в банкомате
    private final int ATM_MAX_SUM = 10000; //Максимальный объем денег(сумма, которая может быть в банкомате)
    private final int MAX_CASH_WITHDRAWAL = 1000; //Максимальная сумма, разрешенная к выдаче
    private int countOperations = 0; // счётчик операций


    public Atm(int currentBalance) { // конструктор
        if (currentBalance > 0 && currentBalance <= ATM_MAX_SUM) {
            this.currentBalance = currentBalance;
        } else {
            this.currentBalance = ATM_MAX_SUM;
        }
    }

    public int cashWithdraw(int cash) { //Выдача денег
        countOperations++;
        if (cash <= currentBalance || cash <= MAX_CASH_WITHDRAWAL) { //
            currentBalance -= cash;
            System.out.println("Выдано " + cash);
            return cash;
        } else {
            System.out.println("Запрошена неверная сумма");
            return 0;
        }
    }

    public int cashDeposit(int deposit) { // Внесение денег на счёт
        countOperations++;
        if (deposit <= (ATM_MAX_SUM - currentBalance)) {
            System.out.println("Внесено: " + deposit);
            return deposit;
        } else {
            System.out.println("Превышен лимит. НЕ внесено: " + (deposit - (ATM_MAX_SUM - currentBalance)));
            return (deposit - (ATM_MAX_SUM - currentBalance));
        }
    }
}