import java.util.Arrays;

public class Main {
    public static void main(String[] args) {

        int[] array = {2, 34, 5, 9, 12, 3, 22, 1, 45, 0, 7};

        int numberOfMin = 0;

        for (int i = 0; i < array.length; i++) {
            if (i == 0) {
                if (array[0] < array[1]) {
                    numberOfMin++;
                }
            } else if (i == array.length - 1) {
                if (array[array.length - 1] < array[array.length - 2]) {
                    numberOfMin++;
                }
            } else if ((array[i] < array[i - 1]) && (array[i] < array[i + 1])) {
                numberOfMin++;
            }
        }

        System.out.println("Размер массива: " + array.length);
        System.out.println("Элементы массива: " + Arrays.toString(array));
        System.out.println("Число локальных минимумов: " + numberOfMin);
    }
}