package ru.inno.ec.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.inno.ec.dto.CourseForm;
import ru.inno.ec.security.details.CustomUserDetails;
import ru.inno.ec.services.CoursesService;

@RequiredArgsConstructor
@Controller
public class CoursesController {

    private final CoursesService coursesService;

    @GetMapping("/courses")
    public String getCoursesPage(@AuthenticationPrincipal CustomUserDetails customUserDetails, Model model) {
        model.addAttribute("role", customUserDetails.getUser().getRole());
        model.addAttribute("coursesList", coursesService.getAllCourses());
        return "courses_page";
    }

    @GetMapping("/courses/{course-id}")
    public String getCoursePage(@PathVariable("course-id") Long courseId, Model model) {
        model.addAttribute("course", coursesService.getCourse(courseId));
        model.addAttribute("notInCourseStudents", coursesService.getNotInCourseStudents(courseId));
        model.addAttribute("inCourseStudents", coursesService.getInCourseStudents(courseId));
        model.addAttribute("notInCourseLessons", coursesService.getNotInCourseLessons());
        model.addAttribute("inCourseLessons", coursesService.getInCourseLessons(courseId));

        return "course_page";
    }

    @PostMapping("/courses")
    public String addCourse(CourseForm course) {
        coursesService.addCourse(course);
        return "redirect:/courses";
    }

    @PostMapping("/courses/{course-id}/update")
    public String updateCourses(@PathVariable("course-id") Long courseId,
                                CourseForm courses) {
        coursesService.updateCourse(courseId, courses);
        return "redirect:/courses/" + courseId;
    }

    @GetMapping("/courses/{course-id}/delete")
    public String deleteCourses(@PathVariable("course-id") Long courseId) {
        coursesService.deleteCourse(courseId);
        return "redirect:/courses/";
    }

    @PostMapping("/courses/{course-id}/students")
    public String addStudentToCourse(@PathVariable("course-id") Long courseId,
                                     @RequestParam("student-id") Long studentId) {
        coursesService.addStudentToCourse(courseId, studentId);
        return "redirect:/courses/" + courseId;
    }

    @PostMapping("/courses/{course-id}/lessons")
    public String addLessonToCourse(@PathVariable("course-id") Long courseId,
                                    @RequestParam("lesson-id") Long lessonId) {
        coursesService.addLessonsToCourse(courseId, lessonId);
        return "redirect:/courses/" + courseId;
    }

}
