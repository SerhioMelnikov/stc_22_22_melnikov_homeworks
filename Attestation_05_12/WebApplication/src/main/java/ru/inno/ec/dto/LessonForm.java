package ru.inno.ec.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.inno.ec.models.Course;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.time.LocalTime;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder

public class LessonForm {

    private String name;
    private String summary;
    private String startTime;
    private String finishTime;
    private Course course;
}
