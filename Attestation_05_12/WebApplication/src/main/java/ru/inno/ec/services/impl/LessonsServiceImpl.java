package ru.inno.ec.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.inno.ec.dto.LessonForm;
import ru.inno.ec.models.Course;
import ru.inno.ec.models.Lesson;
import ru.inno.ec.repositories.LessonsRepository;
import ru.inno.ec.services.LessonsService;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

@Service
@RequiredArgsConstructor

public class LessonsServiceImpl implements LessonsService {

    private final LessonsRepository lessonsRepository;

    @Override
    public List<Lesson> getAllLessons() {
        return lessonsRepository.findAllByStateNot(Lesson.State.DELETED);
    }

    @Override
    public Lesson getLesson(Long lessonId) {
        return lessonsRepository.findById(lessonId).orElseThrow();
    }

    @Override
    public void addLesson(LessonForm lesson) {
        Lesson newLesson = Lesson.builder()
                .name(lesson.getName())
                .summary(lesson.getSummary())
                .startTime(LocalTime.parse(lesson.getStartTime()))
                .finishTime(LocalTime.parse(lesson.getFinishTime()))
                .state(Lesson.State.NOT_CONFIRMED)
                .build();
        lessonsRepository.save(newLesson);

    }

    @Override
    public void deleteLesson(Long lessonId) {
        Lesson lessonForDel = lessonsRepository.findById(lessonId).orElseThrow();
        lessonForDel.setState(Lesson.State.DELETED);
        lessonsRepository.save(lessonForDel);
    }

    @Override
    public void updateLesson(Long lessonId, LessonForm updateLesson) {
        Lesson lessonForUpd = lessonsRepository.findById(lessonId).orElseThrow();
        lessonForUpd.setName(updateLesson.getName());
        lessonForUpd.setSummary(updateLesson.getSummary());
        lessonForUpd.setStartTime(LocalTime.parse(updateLesson.getStartTime()));
        lessonForUpd.setFinishTime(LocalTime.parse(updateLesson.getFinishTime()));
        lessonsRepository.save(lessonForUpd); //check
    }
}
