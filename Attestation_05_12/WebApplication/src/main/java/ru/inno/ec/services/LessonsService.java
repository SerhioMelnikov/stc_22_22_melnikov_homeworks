package ru.inno.ec.services;


import ru.inno.ec.dto.LessonForm;
import ru.inno.ec.models.Lesson;
import java.util.List;


public interface LessonsService {
    Lesson getLesson(Long id);
    List<Lesson> getAllLessons();
    void addLesson(LessonForm lesson);
    void deleteLesson(Long lessonId);
    void updateLesson(Long lessonId, LessonForm lesson);
}
