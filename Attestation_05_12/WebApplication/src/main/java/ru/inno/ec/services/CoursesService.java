package ru.inno.ec.services;

import ru.inno.ec.dto.CourseForm;
import ru.inno.ec.models.Course;
import ru.inno.ec.models.Lesson;
import ru.inno.ec.models.User;

import java.util.List;

public interface CoursesService {

    Course getCourse(Long courseId);
    List<Course> getAllCourses();
    void addCourse(CourseForm course);
    void updateCourse(Long courseId, CourseForm course);
    void deleteCourse(Long courseId);
    public void addStudentToCourse(Long courseId, Long studentId);
    public void addLessonsToCourse(Long courseId, Long lessonId);
    List<Lesson> getNotInCourseLessons();
    List<Lesson> getInCourseLessons(Long courseId);
    List<User> getNotInCourseStudents(Long courseId);
    List<User> getInCourseStudents(Long courseId);
}
