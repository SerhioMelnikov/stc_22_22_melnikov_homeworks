
insert into driver (first_name, last_name, phone_number, experience, age, driving_license, category, rating)
values ('Ivan', 'Petrov', '1234567', 20,  42, true, 'B', 4.83);
insert into driver (first_name, last_name, phone_number, experience, age, driving_license, category, rating)
values ('Yuriy', 'Kuznetsov', '3451267', 29,  62, true, 'BC', 4.75);
insert into driver (first_name, last_name, phone_number, experience, age, driving_license, category, rating)
values ('Yuliya', 'Ivanova', '6123457', 14,  35, true, 'B', 4.8);
insert into driver (first_name, last_name, phone_number, experience, age, driving_license, category, rating)
values ('Alexandr', 'Popov', '3541267', 8,  45, true, 'B', 4.6);
insert into driver (first_name, last_name, phone_number, experience, age, driving_license, category, rating)
values ('Petr', 'Popov', '3542267', 10,  40, true, 'B', 4.81);

insert into car (car_type, color, car_number, id_car_owner)
values ('Skoda', 'white', 'OT346E', 2),
       ('Ford', 'black', 'OA368T', 4),
       ('Skoda', 'white', 'AA140E', 3),
       ('Lada', default, 'HA123M', 2),
       ('Kia', 'red', 'XMT346K', 4);

insert into ride (car_id, driver_id, journey_date, start_time, finish_time)
values (3, 4, '2022-11-01','12:45', '12:59'),
       (4, 1, '2022-10-29', '20:03', '20:19'),
       (3, 5, '2022-11-08', '07:07', '08:09'),
       (2, 3, '2022-11-13', '15:23', '15:33'),
       (1, 1, '2022-11-01', '00:45', '01:02');