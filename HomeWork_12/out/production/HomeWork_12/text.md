create table driver (
id bigserial,
first_name char(20) default 'DEFAULT FIRST_NAME',
last_name char(20),
phone_number integer unique,
experience integer,
age integer check(age >= 0 and age<=100),
driving_license bool not null,
category char(5),
rating integer check (rating >= 0 and rating<=5)
)