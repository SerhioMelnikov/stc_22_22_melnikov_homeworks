import java.util.ListIterator;

public class Main {

    public static String mergeDocuments(Iterable<String> documents) {
        StringBuilder mergedDocument = new StringBuilder();
        Iterator<String> documentsIterator = documents.iterator();
        while (documentsIterator.hasNext()) {
            mergedDocument.append(documentsIterator.next() + " ");
        }
        return mergedDocument.toString();
    }

    public static void main(String[] args) {
        List<String> stringList = new ArrayList<>();

        stringList.add("Hello");
        stringList.add("Bye");
        stringList.add("Fine");
        stringList.add("C++");
        stringList.add("PHP");
        stringList.add("Cobol");
        stringList.add("JS");
        stringList.remove("C++");
        stringList.removeAt(1);

        String documents = mergeDocuments(stringList);
        System.out.println(stringList.size());
        System.out.println(documents);
    }


}