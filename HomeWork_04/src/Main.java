public class Main {
    public static void evenDigits(int[] a) {
        for (int i = 0; i < a.length; i++) {
            if (a[i] % 2 == 0) {
                System.out.print(a[i] + " ");
            }
        }
    }

    public static int sumOfDigits(int from, int to) {
        int sum = 0;
        if (from < to) {
            for (int i = from; i <= to; i++) {
                sum += i;
            }
            return sum;
        } else {
            return -1;
        }
    }

    public static int toInt(int[] b) {
        int result = 0;
        int a = 1;
        for (int i = b.length - 1; i >= 0; i--) {
            result += b[i] * a;
            a *= 10;
        }
        return result;
    }

    public static void main(String[] args) {
        int sum1 = sumOfDigits(3, 14);
        System.out.println("Сумма чисел в заденном интервале: " + sum1); // сумма чисел в заданном интервале

        int[] array = {8, 3, 12, 0, 5, 44, 6, 9};
        evenDigits(array); // чётные элементы заданного массива

        int[] array2 = {3, 5, 9};
        int x = toInt(array2);
        System.out.println("\n" + "Искомое число: " + x); // число из массива в int

    }
}




