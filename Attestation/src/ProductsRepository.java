import java.io.IOException;
import java.util.List;
import java.util.Optional;

public interface ProductsRepository {

    Product findById(Integer id)throws IOException;
    List<Product> findAllByTitleLike(String title) throws IOException;
    void update(Product product) throws IOException;

}
