import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ProductsRepositoryFileBasedImpl implements ProductsRepository {

    private final String fileName;
    public ProductsRepositoryFileBasedImpl(String fileName) {
        this.fileName = fileName;
    }
    private static final Function<String, Product> stringToProductMapper = currentProduct -> {
        String[] parts = currentProduct.split("\\|");
        Integer id = Integer.parseInt(parts[0]);
        String productName = parts[1];
        Double cost = Double.parseDouble(parts[2]);
        Integer amount = Integer.parseInt(parts[3]);
        return new Product(id, productName, cost, amount);
    };
    private static final Function<Product, String> productToStringMapper = product -> {
        return product.getId().toString()
                + "|" + product.getProductName()
                + "|" + product.getCost()
                + "|" + product.getAmount();
    };
    @Override
    public Product findById(Integer id) {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            return reader.lines()
                    .map(stringToProductMapper)
                    .filter(product -> product.getId().equals(id))
                    .findFirst()
                    .orElseThrow(NoSuchElementException::new);
        } catch (IOException e) {
            throw new UnsuccessfulWorkWithFileException(e);
        }
    }
    @Override
    public List<Product> findAllByTitleLike(String title) {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            return reader.lines()
                    .map(stringToProductMapper)
                    .filter(p -> p.getProductName().toLowerCase().contains(title.toLowerCase()))
                    .toList();
        } catch (IOException e) {
            throw new UnsuccessfulWorkWithFileException (e);
        }
    }
    @Override
    public void update(Product product)  {
        List<Product> productsList = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            productsList = reader.lines()
                    .map(stringToProductMapper)
                    .toList();
            Product currentProduct = productsList
                    .stream()
                    .filter(p -> p.getId().equals(product.getId()))
                    .findFirst()
                    .orElseThrow(NoSuchElementException::new);
            Product newProduct = new Product(currentProduct.getId(),product.getProductName(),
                    product.getCost(),
                    product.getAmount());
            List<Product> newProductList = productsList
                    .stream()
                    .map(p -> {
                        if (Objects.equals(p.getId(), newProduct.getId())) {
                            return newProduct;
                        }
                        return p;
                    })
                    .toList();
            saveList(newProductList);
        } catch (IOException e) {
            throw new UnsuccessfulWorkWithFileException (e);
        }
    }
    public void saveList(List<Product> products) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileName, false))) {
            StringBuilder stringProduct = new StringBuilder();
            for (Product prod : products) {
                stringProduct.append(productToStringMapper.apply(prod)).append("\n");
            }
            writer.write(stringProduct.toString());
        } catch (IOException e) {
            throw new UnsuccessfulWorkWithFileException(e);
        }
    }
}
