import java.util.Objects;

public class Product {
    private Integer id;
    private String productName;
    private Double cost;
    private Integer amount;
    public Product(Integer id, String productName, Double cost, Integer amount) {
        this.id = id;
        this.productName = productName;
        this.cost = cost;
        this.amount = amount;
    }
    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", productName='" + productName + '\'' +
                ", cost=" + cost +
                ", amount=" + amount +
                '}';
    }
    public Integer getId() {
        return id;
    }
    public String getProductName() {
        return productName;
    }
    public Double getCost() {
        return cost;
    }
    public Integer getAmount() {
        return amount;
    }
    public void setCost(Double cost) {
        this.cost = cost;
    }
    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return Objects.equals(id, product.id) && Objects.equals(productName, product.productName) && Objects.equals(cost, product.cost) && Objects.equals(amount, product.amount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, productName, cost, amount);
    }
}
