import java.io.IOException;

public class Main {
    public static void main(String[] args) {

        ProductsRepository productsRepository = new ProductsRepositoryFileBasedImpl("input.txt");
        try {
            System.out.println(productsRepository.findAllByTitleLike("��"));
            System.out.println("***************");
            System.out.println(productsRepository.findById(3));
            System.out.println("***************");
            Product milk = productsRepository.findById(2);

            milk.setCost(155.0);
            productsRepository.update(milk);
            System.out.println(productsRepository.findById(2));

        } catch (IOException ex) {
            System.out.println("Error");
        }
    }
}