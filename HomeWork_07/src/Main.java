public class Main {

    static void completeAllTasks(Task[] tasks) {
        for (Task i : tasks) {
            i.complete();
        }
    }

    public static void main(String[] args) {
        Task oddNumbers = new OddNumbersPrintTask(1, 10);
        Task evenNumbers = new EvenNumbersPrintTask(5, 15);
        Task[] tasks = {oddNumbers, evenNumbers};
        completeAllTasks(tasks);


    }
}