public class OddNumbersPrintTask extends AbstractNumbersPrintTask implements Task {
    public OddNumbersPrintTask(int from, int to) {
        super(from, to);
    }

    @Override
    public void complete() {
        for (int i = from; i <= to; i++) {
            if (i % 2 == 1) {
                System.out.print(i + " ");
            }
        }
    }
}