public abstract class AbstractNumbersPrintTask {
    int from, to;
    abstract void complete();
    public AbstractNumbersPrintTask(int from, int to) {
        this.from = from;
        this.to = to;
    }


}
