package ru.inno.ec.models;

import javax.persistence.*;

import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@EqualsAndHashCode(exclude = "course")
@ToString(exclude = "course")

public class Lesson {

    public enum State {
        NOT_CONFIRMED, CONFIRMED, DELETED
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @Column(length = 1000)
    private String summary;

    @DateTimeFormat(pattern = "hh:mm")
    @Column(name = "start_time", columnDefinition = "time default '12:00'")
    private LocalTime startTime;

    @Column(name = "finish_time", columnDefinition = "time default '12:00'")
    private LocalTime finishTime;

    @ManyToOne
    @JoinColumn(name = "course_id")
    private Course course;

    @Enumerated(value = EnumType.STRING)
    private Lesson.State state;
}
