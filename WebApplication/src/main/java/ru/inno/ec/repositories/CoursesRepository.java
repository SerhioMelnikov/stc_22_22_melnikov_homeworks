package ru.inno.ec.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.inno.ec.models.Course;
import ru.inno.ec.models.User;

import java.util.List;

public interface CoursesRepository extends JpaRepository<Course, Long> {

    List<Course> findAllByStateNot(Course.State state);

}
