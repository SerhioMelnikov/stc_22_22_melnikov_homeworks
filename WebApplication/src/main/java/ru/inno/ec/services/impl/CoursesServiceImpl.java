package ru.inno.ec.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.inno.ec.dto.CourseForm;
import ru.inno.ec.models.Course;
import ru.inno.ec.models.Lesson;
import ru.inno.ec.models.User;
import ru.inno.ec.repositories.CoursesRepository;
import ru.inno.ec.repositories.LessonsRepository;
import ru.inno.ec.repositories.UsersRepository;
import ru.inno.ec.services.CoursesService;

import java.time.LocalDate;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CoursesServiceImpl implements CoursesService {

    private final CoursesRepository coursesRepository;
    private final LessonsRepository lessonsRepository;
    private final UsersRepository usersRepository;


    @Override
    public List<Course> getAllCourses() {
        return coursesRepository.findAllByStateNot(Course.State.DELETED);
    }

    @Override
    public Course getCourse(Long courseId) {
        return coursesRepository.findById(courseId).orElseThrow();
    }

    @Override
    public void addCourse(CourseForm course) {
        Course newCourse = Course.builder()
                .title(course.getTitle())
                .description(course.getDescription())
                .start(LocalDate.parse(course.getStart()))
                .finish(LocalDate.parse(course.getFinish()))
                .state(Course.State.NOT_CONFIRMED)
                .build();
        coursesRepository.save(newCourse);
    }

    @Override
    public void updateCourse(Long courseId, CourseForm updateCourse) {
        Course courseForUpd = coursesRepository.findById(courseId).orElseThrow();
        courseForUpd.setTitle(updateCourse.getTitle());
        courseForUpd.setDescription(updateCourse.getDescription());
        courseForUpd.setStart(LocalDate.parse(updateCourse.getStart()));
        courseForUpd.setFinish(LocalDate.parse(updateCourse.getFinish()));
        coursesRepository.save(courseForUpd);
    }

    @Override
    public void deleteCourse(Long courseId) {
        Course courseForDel = coursesRepository.findById(courseId).orElseThrow();
        courseForDel.setState(Course.State.DELETED);
        coursesRepository.save(courseForDel);
    }

    @Override
    public void addLessonsToCourse(Long courseId, Long lessonId) {
        Course course = coursesRepository.findById(courseId).orElseThrow();
        Lesson lesson = lessonsRepository.findById(lessonId).orElseThrow();
        lesson.setCourse(course);
        lessonsRepository.save(lesson);
    }

    @Override
    public void addStudentToCourse(Long courseId, Long studentId) {
        Course course = coursesRepository.findById(courseId).orElseThrow();
        User student = usersRepository.findById(studentId).orElseThrow();
        student.getCourses().add(course); //check
        usersRepository.save(student);
    }
    @Override
    public List<Lesson> getNotInCourseLessons() {
        return lessonsRepository.findAllByCourseNull();
    }

    @Override
    public List<Lesson> getInCourseLessons(Long courseId) {
        Course course = coursesRepository.findById(courseId).orElseThrow();
        return lessonsRepository.findAllByCourse(course);

    }

    @Override
    public List<User> getNotInCourseStudents(Long courseId){
        Course course = coursesRepository.findById(courseId).orElseThrow();
        return usersRepository.findAllByCoursesNotContains(course);
    }

    @Override
    public List<User> getInCourseStudents(Long courseId) {
        Course course = coursesRepository.findById(courseId).orElseThrow();
        return usersRepository.findAllByCoursesContains(course);
    }
}
