import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        Map<String, Integer> people = new HashMap<>();
        String names = "Piter John David John Andrew Piter Robert John";
        String[] parts = names.split("\\s");
        for (String string : parts) {
            if (people.containsKey(string)) {
                people.put(string, people.get(string) + 1);
            } else {
                people.put(string, 1);
            }
        }
        System.out.println(people);
        int max = Collections.max(people.values());
        for (String key :
                people.keySet()) {
            if (people.get(key) == max) {
                System.out.println(key + " " + people.get(key));
            }
        }
    }
}